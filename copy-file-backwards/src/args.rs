use clap::Parser;

#[derive(Parser, Debug)]
pub struct Args {
    /// Input file path
    pub input: std::path::PathBuf,
    /// Output file path
    pub output: std::path::PathBuf,
    /// Read buffer size
    #[arg(long, short, default_value_t = 16)]
    pub buffer_size: usize,
}

