use std::{
    fs::File,
    io::{
        Write,
        Read,
        Result,
        Seek,
        SeekFrom,
    },
    cmp::{
        max,
        min,
    },
};
use clap::Parser;

mod args;
use args::Args;

fn main() -> Result<()> {
    let args = Args::parse();
    
    let mut input_file = File::open(args.input)?;

    let mut output_file = File::create(args.output)?;
    
    let file_size = input_file.metadata()?
        .len();
    let buffer_size = min(
        max(args.buffer_size, 2) * 8,
        file_size as usize
    );
    let mut buffer = vec![0; buffer_size];

    input_file.seek(
        SeekFrom::End(
            -(buffer_size as i64)
        )
    )?;

    let mut remaining_file_size = file_size as usize;
    while remaining_file_size > 0 {
        remaining_file_size -= buffer_size;
        let bytes_read = input_file.read(&mut buffer)?;
        buffer[..bytes_read].reverse();
        output_file.write(&buffer[..bytes_read])?;

        let next_offset = buffer_size
            + min(buffer_size, remaining_file_size);
        input_file.seek(
            SeekFrom::Current(
                -(next_offset as i64)
            )
        )?;
    }

    Ok(())
}

