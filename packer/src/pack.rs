use std::{
    collections::HashMap, fs::File, io::{
        Read,
        Write
    },
    os::unix::fs::PermissionsExt,
    path::{
        Path,
        PathBuf
    },
    time::SystemTime
};

use openssl::symm::{encrypt, decrypt, Cipher};
use openssl::hash::{hash, MessageDigest};

use serde::{
    Deserialize,
    Serialize
};

use crate::file_utils::create_file_with_dirs;

pub struct Pack {
    path: PathBuf,
    password: String,
    files: HashMap<PathBuf, PackFile>,
}

#[derive(Debug)]
pub enum PackError {
    BincodeError(bincode::Error),
    IoError(std::io::Error),
    OpenSSLError(openssl::error::ErrorStack),
}

impl Pack {
    pub fn new(pack_path: &Path, password: &str) -> Result<Pack, PackError> {
        if pack_path.exists() {
            let mut file = File::open(pack_path).map_err(PackError::IoError)?;
            let mut encrypted_data = Vec::new();
            file.read_to_end(&mut encrypted_data).map_err(PackError::IoError)?;
            let data = decrypt_data(&encrypted_data, &password)
                .map_err(PackError::OpenSSLError)?;
            let files = bincode::deserialize(&data)
                .map_err(PackError::BincodeError)?;
            Ok(Pack {
                path: pack_path.to_path_buf(),
                password: password.to_string(),
                files,
            })
        } else {
            let files = HashMap::new();
            Ok(Pack {
                path: pack_path.to_path_buf(),
                password: password.to_string(),
                files,
            })
        }
    }

    pub fn add(&mut self, file_path: &Path) -> Result<(), std::io::Error> {
        let mut file = File::open(file_path)?;
        let mut content = Vec::new();
        file.read_to_end(&mut content)?;
        if let Ok(metadata) = file.metadata() {
            let access_time = metadata.accessed().ok();
            let modification_time = metadata.modified().ok();
            let permissions = Some(metadata.permissions().mode());
            self.files.insert(
                file_path.to_path_buf(),
                PackFile {
                    content,
                    access_time,
                    modification_time,
                    permissions,
                }
            );
        } else {
            self.files.insert(
                file_path.to_path_buf(),
                PackFile {
                    content,
                    access_time: None,
                    modification_time: None,
                    permissions: None,
                }
            );
        }
        Ok(())
    }

    pub fn unpack(&mut self, file_path: &Path) -> Result<(), std::io::Error> {
        if let Some(pack_file) =  self.files.get(file_path) { 
            let mut file = create_file_with_dirs(file_path)?;
            file.write_all(&pack_file.content)?;
            let times = std::fs::FileTimes::new()
                .set_accessed(pack_file.access_time.unwrap_or(SystemTime::now()))
                .set_modified(pack_file.modification_time.unwrap_or(SystemTime::now()));
            file.set_times(times)?;
            let permissions = std::fs::Permissions::from_mode(
                pack_file.permissions.unwrap_or(0o644)
            );
            file.set_permissions(permissions)?;
            self.files.remove(file_path);
            Ok(())
        } else {
            Err(std::io::Error::new(
                std::io::ErrorKind::InvalidData,
                format!("{:?} not found in the pack", file_path)
            ))
        }
    }

    pub fn list_files(&self) -> Vec<PathBuf> {
        self.files.keys().cloned().collect()
    }

    pub fn write(&self) -> Result<(), PackError> {
        let mut file = create_file_with_dirs(&self.path).map_err(PackError::IoError)?;
        let data = bincode::serialize(&self.files).map_err(PackError::BincodeError)?;
        let encrypted_data = encrypt_data(&data, &self.password)
            .map_err(PackError::OpenSSLError)?; 
        file.write_all(&encrypted_data).map_err(PackError::IoError)?;
        Ok(())
    }
}

fn encrypt_data(data: &Vec<u8>, password: &str) ->
Result<Vec<u8>, openssl::error::ErrorStack> {
    let key = hash_and_salt_password(password)?;
    let cipher = Cipher::aes_256_cbc(); 
    let iv = [0; 16];

    let encrypted = encrypt(cipher, &key, Some(&iv), data)?;
    Ok(encrypted)
}

fn decrypt_data(encrypted: &Vec<u8>, password: &str) ->
Result<Vec<u8>, openssl::error::ErrorStack> {
    let key = hash_and_salt_password(password)?;
    let cipher = Cipher::aes_256_cbc();
    let iv = [0; 16];

    let data = decrypt(cipher, &key, Some(&iv), encrypted)?;
    Ok(data)
}

#[derive(Serialize, Deserialize)]
struct PackFile {
    content: Vec<u8>,
    access_time: Option<SystemTime>,
    modification_time: Option<SystemTime>,
    permissions: Option<u32>,
}

fn hash_and_salt_password(password: &str) ->
Result<[u8; 32], openssl::error::ErrorStack> {
    let key = format!("supersecretsalt{}", password);
    let digest = hash(MessageDigest::sha256(), key.as_bytes())?;
    let mut hashed_key = [0u8; 32];
    hashed_key.copy_from_slice(&digest);
    Ok(hashed_key)
}

#[test]
fn test_data_encryption() {
    let data = Vec::from("sample data");
    let password = "qwerty123";
    let encrypted_data = encrypt_data(&data, &password).unwrap();
    let decrypted_data = decrypt_data(&encrypted_data, &password).unwrap();
    assert_eq!(data, decrypted_data);
}

