use std::{
    ffi::OsStr,
    fs::{
        create_dir_all,
        File,
    },
    io::{
        self,
        Read
    },
    path::{
        Path,
        PathBuf
    }
};

pub fn get_file_path_list(file_paths: &Vec<PathBuf>, read_file_paths: bool) ->
    Result<Vec<PathBuf>, io::Error> {
    let mut file_path_list: Vec<PathBuf> = Vec::new();
    if read_file_paths {
        for f in file_paths {
            file_path_list.append(&mut read_file_path_list(&f)?);
        }
    } else {
        file_path_list = file_paths.to_vec();
    }
    
    Ok(file_path_list)
}

pub fn create_file_with_dirs(path: &Path) -> Result<File, io::Error> {
    if let Some(parent) = path.parent() {
        create_dir_all(parent)?;
    }
    let file = File::create(path)?;
    Ok(file)
}

pub fn validate_file_path(file_path: &Path) ->
Result<(), io::Error> {
    if file_path.display().to_string().contains("..") {
        return Err(io::Error::new(
            io::ErrorKind::InvalidInput,
            format!("Invalid file path: {:?}", file_path)
        ))
    }
    Ok(())
}

pub fn validate_file_path_existence(file_path: &Path) ->
Result<(), io::Error> {
    if !file_path.exists()  {
        return Err(io::Error::new(
            io::ErrorKind::InvalidInput,
            format!("File doesn't exist: {:?}", file_path)
        ))
    }
    Ok(())
}

fn read_file_path_list(file_path: &Path) -> Result<Vec<PathBuf>, io::Error> {
    let extension = file_path.extension().and_then(OsStr::to_str);
    match extension {
        Some("txt") | None => Ok(read_plain_text(
            read_file_content(file_path)?
        )),
        Some("json") => Ok(read_json(
            &read_file_content(file_path)?
        )?),
        _ => Ok(vec![file_path.to_path_buf()]),
    }
}

fn read_file_content(file_path: &Path) -> Result<String, io::Error> {
    validate_file_path_existence(file_path)?;
    let mut file = File::open(file_path)?;
    let mut content = String::new();
    file.read_to_string(&mut content)?;
    Ok(content)
}

fn read_plain_text(mut content: String) -> Vec<PathBuf> {
    let delimiters = vec![',', ';', '\n'];
    for d in delimiters {
        content = content.replace(d, " ");
    }
    content.split_whitespace()
        .map(|a| PathBuf::from(a))
        .collect()
}

fn read_json(content: &str) -> Result<Vec<PathBuf>, io::Error> {
    let file_list = serde_json::from_str::<Vec<String>>(content)?;
    Ok(file_list.into_iter()
        .map(|a| PathBuf::from(a))
        .collect())
}

#[test]
fn test_validate_file_path() {
    let file_path = PathBuf::from("Cargo.toml");
    assert!(validate_file_path(&file_path).is_ok());

    let file_path = PathBuf::from("src/../Cargo.toml");
    assert!(validate_file_path(&file_path).is_err());
}

#[test]
fn test_validate_file_path_existence() {
    let file_path = PathBuf::from("Cargo.toml");
    assert!(validate_file_path_existence(&file_path).is_ok());

    let file_path = PathBuf::from("no_such_file.json");
    assert!(validate_file_path_existence(&file_path).is_err());
}

#[test]
fn test_read_plain_text() {
    let content = String::from(r#"
    one.txt, two.json,three.png four    five.jpg
    six.yml; seven.rs;eight.zip,
    .nine;
      ten.toml
    "#);
    let result = read_plain_text(content);
    let expected_result: Vec<PathBuf> = vec![
        "one.txt", "two.json",
        "three.png", "four",
        "five.jpg", "six.yml",
        "seven.rs", "eight.zip",
        ".nine", "ten.toml"
    ].into_iter()
        .map(|a| PathBuf::from(a))
        .collect();
    assert_eq!(result, expected_result);
}

#[test]
fn test_read_empty_plain_text() {
    let content = String::from(" ");
    let result = read_plain_text(content);
    let expected_result: Vec<PathBuf> = vec![];
    assert_eq!(result, expected_result);
}

#[test]
fn test_read_json() {
    let content = String::from(r#"
    [
        "one.txt",
        "two.json",
        "three.png"
    ]
    "#);
    let result = read_json(&content).unwrap();
    let expected_result: Vec<PathBuf> = vec![
        "one.txt", "two.json", "three.png"
    ].into_iter()
        .map(|a| PathBuf::from(a))
        .collect();
    assert_eq!(result, expected_result);
}

#[test]
fn test_read_invalid_json() {
    let content = String::from(r#"
    {
    "#);
    let result = read_json(&content);
    assert!(result.is_err());

    let content = String::from(r#"
    {
        "a": {
            "files": [
                "a.txt"
            ]
        }
    }
    "#);
    let result = read_json(&content);
    assert!(result.is_err());
}

