use std::path::PathBuf;

use clap::{Parser, Subcommand};

#[derive(Parser, Debug)]
pub struct Args {
    #[clap(subcommand)]
    pub command: Command,
}

#[derive(Subcommand, Debug)]
pub enum Command {
    /// Add files to a pack
    Add {
        /// Path to the pack
        pack_path: PathBuf,
        /// Pack password
        #[arg(long, short)]
        password: String,
        /// List of files to add to the pack
        #[arg(long, short, num_args = 1..)]
        files: Vec<PathBuf>,
        /// Whether to read the file list from the files
        #[arg(long, short)]
        read_file_paths: bool,
    },
    /// Unpack files from a pack
    Unpack {
        /// Path to the pack
        pack_path: PathBuf,
        /// Pack password
        #[arg(long, short)]
        password: String,
        /// List of files to unpack from the pack
        #[arg(long, short, num_args = 1..)]
        files: Vec<PathBuf>,
        /// Whether to read the file list from the files
        #[arg(long, short)]
        read_file_paths: bool,
    },
    /// List files in a pack
    List {
        /// Path to the pack
        pack_path: PathBuf,
        /// Pack password
        #[arg(long, short)]
        password: String,
    },
}

