use std::path::{
    Path,
    PathBuf
};

use clap::Parser;

mod args;
mod file_utils;
mod pack;

use args::{
    Args,
    Command
};
use file_utils::{
    get_file_path_list,
    validate_file_path,
    validate_file_path_existence
};
use pack::{
    Pack,
    PackError,
};

fn add(pack_path: &Path, password: &str,
    files: &Vec<PathBuf>, read_file_paths: bool) -> Result<(), PackError> {
   let file_list = get_file_path_list(&files, read_file_paths)
        .map_err(PackError::IoError)?;
    for f in files {
        validate_file_path(f).map_err(PackError::IoError)?;
        validate_file_path_existence(f).map_err(PackError::IoError)?;
    }
    let mut pack = Pack::new(pack_path, password)?;
    for f in file_list {
        pack.add(&f).map_err(PackError::IoError)?;
    }
    pack.write()?;
    Ok(())
}

fn unpack(pack_path: &Path, password: &str,
    files: &Vec<PathBuf>, read_file_paths: bool) -> Result<(), PackError> {
    let file_list = get_file_path_list(&files, read_file_paths)
        .map_err(PackError::IoError)?;
    for f in files {
        validate_file_path(f).map_err(PackError::IoError)?;
    }
    validate_file_path_existence(pack_path).map_err(PackError::IoError)?;
    let mut pack = Pack::new(pack_path, password)?;
    for f in file_list {
        pack.unpack(&f).map_err(PackError::IoError)?;
    }
    pack.write()?;
    Ok(())
}

fn list(pack_path: &Path, password: &str) -> Result<(), PackError> {
    validate_file_path_existence(pack_path).map_err(PackError::IoError)?;
    let pack = Pack::new(pack_path, password)?;
    let file_list = pack.list_files();
    for f in file_list {
        println!("{}", f.display().to_string());
    }
    Ok(())
}

fn main() -> Result<(), PackError> {
    let args = Args::parse();

    match args.command {
        Command::Add { pack_path, password, files, read_file_paths } => {
            add(&pack_path, &password,
                &files, read_file_paths
            )
        },
        Command::Unpack { pack_path, password, files, read_file_paths } => {
            unpack(&pack_path, &password,
                &files, read_file_paths
            )
        },
        Command::List { pack_path, password } => {
            list(&pack_path, &password)
        }
    }
}

