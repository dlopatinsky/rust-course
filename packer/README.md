# Packer

## CLI interface
### add command
Parameters:
- pack path
- pack password
- files to add
Usage example:
```sh
packer add my_cool.pak --pass=qwerty --files=cat.jpg
```
### unpack command
- pack path
- pack password
- files to unpack
Usage example:
```sh
packer unpack my_cool.pak --pas=qwerty --files=cat.jpg
```

