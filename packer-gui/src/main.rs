use eframe::egui;

mod app;

use app::App;

fn main() -> Result<(), eframe::Error> {
    env_logger::init();
    let options = eframe::NativeOptions {
        viewport: egui::ViewportBuilder::default()
            .with_inner_size([690.0, 420.0]),
        ..Default::default()
    };
    eframe::run_native(
        "Packer",
        options,
        Box::new(|_cc| {
            Box::<App>::default()
        }),
    )
}

